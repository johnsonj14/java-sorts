import java.util.ArrayDeque;

/**
 * Contains a recursive and non-recursive quick sort algorithm, as well as a
 * insertion sort algorithm
 */
public class SortTest
{
   /**
    * Recursive version of Quicksort
    * 
    * @param values is the array of integers to be sorted
    */
   public static void quickSortRecursive(int[] values)
   {
      quickSortRecursive(values, 0, values.length - 1);
   }


   /**
    * Recursive version of Quicksort
    * 
    * @param values is the array of integers to be sorted
    * @param left is the left of the array
    * @param right is the right of the array
    */
   private static void quickSortRecursive(int[] values, int left, int right)
   {
      // Get a pivot value.
      int pivot = median3(values, left, right);
      int low = left;
      int high = right;

      /*
       * Partition the array.
       * Identify a number from left side which is greater then the pivot 
       * value, and also identify a number from right side which is 
       * less then the pivot value. Then swap them.
       */
      while (low <= high)
      {
         while (values[low] < pivot)
         {
            low++;
         }
         while (values[high] > pivot)
         {
            high--;
         }
         if (low <= high)
         {
            swap(values, low, high);
            low++;
            high--;
         }
      }
      // recursively call sort for particular parts of the array
      if (left < high)
         quickSortRecursive(values, left, high);
      if (low < right)
         quickSortRecursive(values, low, right);
   }
   

   /**
    * Non-recursive version of Quicksort
    * 
    * @param values is the array of integers to be sorted
    */
   public static void quickSort(int[] values)
   {
      // Stack to simulate recursion calls.
      ArrayDeque<Integer> stack = new ArrayDeque<Integer>();
      // The start and end of the partition.
      int left;
      int right;
      int p;
      // Push the initial values of the stack, i.e. 0 for the first index of 
      //    values[] and the length of values[] as the last index of values[].
      stack.push(0);
      stack.push(values.length);
      
      // While the stack is not empty, sort.
      while (!stack.isEmpty())
      {
         // Get the start and end of the partition.
         right = stack.pop();
         left = stack.pop();
         
         // If from the start to the end, it's not already sorted. So 
         //    sort this section.
         if (right - left >= 2)
         {
            // Break up the section of the array to be sorted, and partition it.
            p = left + ((right - left) / 2);
            p = partition(values, p, left, right);
            
            // Push the indexes onto the stack in such a way that they are
            //    analyzed correctly in the following iterations.
            stack.push(p + 1);
            stack.push(right);
            stack.push(left);
            stack.push(p);
         } // if
      } // while
   } // quickSort
   

   /**
    * Iterative version of Insertion Sort
    * 
    * @param values is the array of integers to be sorted
    */
   public static void insertionSort(int[] values)
   {
      int j;
      int currentValue;
      // for every value in values, shift values[i] to the
      //    appropriate place in the sorted array
      for (int i = 1; i < values.length; i++)
      {
         currentValue = values[i];
         // shift the values in values until we created an open
         //    index for the currentValue
         for (j = i; j > 0 && currentValue < values[j-1]; j--)
         {
            values[j] = values[j-1];
         }
         // set the currentValue to the appropriate index in sorted values
         values[j] = currentValue;
      }
   }
   
   
   /**
    * Quick Sort algorithm that uses two threads to accomplish sorting.
    * 
    * @param values is the array of integers to be sorted
    */
   public static void quickSortThreaded(int[] values)
   {
      // Get a pivot value.
      int pivot = median3(values, 0, values.length - 1);
      int low = 0;
      int high = values.length - 1;

      /*
       * Partition the array.
       * Identify a number from left side which is greater then the pivot 
       * value, and also identify a number from right side which is 
       * less then the pivot value. Then swap them.
       */
      while (low <= high)
      {
         while (values[low] < pivot)
         {
            low++;
         }
         while (values[high] > pivot)
         {
            high--;
         }
         if (low <= high)
         {
            swap(values, low, high);
            low++;
            high--;
         }
      }
      // Create two threads, and sort the two sides of the array.
      Thread loThread = new Thread( () -> { quickSortRecursive(values, 0, pivot - 1); } );
      Thread hiThread = new Thread( () -> { quickSortRecursive(values, pivot, values.length - 1); } );
      loThread.start();
      hiThread.start();
   }
   

   /**
    * Swaps two elements in an array
    * 
    * @param array is the array
    * @param i is one element
    * @param j is the other element
    */
   private static void swap(int[] array, int i, int j)
   {
      int temp = array[i];
      array[i] = array[j];
      array[j] = temp;
   }

   
   /**
    * Used to partition an array in quick sort
    * 
    * @param values is the array
    * @param position is the position in the array
    * @param start is the start of the partition
    * @param end is the end of the partition
    * @return partition index
    */
   private static int partition(int[] values, int position, int start, int end)
   {
      int low = start;
      int high = end - 2;
      int pivot = values[position];
      int index;
      
      // Based on the value of the pivot, make sure that the values are
      //    on the correct side of the pivot.
      swap(values, position, end - 1);
      while (low < high)
      {
         if (values[low] < pivot)
         {
            low++;
         }
         else if (values[high] >= pivot)
         {
            high--;
         }
         else
         {
            swap(values, low, high);
         }
      }
      index = high;
      if (values[high] < pivot)
      {
         index++;
      }
      swap(values, end - 1, index);
      return index;
   }
   
   
   /**
    * Median-of-three partitioning, as described in the Weiss text, picks 
    * three values (left, center, and right), and chooses the median of 
    * the three. Used to get a more efficient pivot.
    * 
    * @param values is the array
    * @param left is the first element
    * @param right is the last element
    * @return the median of the three values
    */
   private static int median3(int[] values, int left, int right)
   {
      // set center to the middle index
      int center = (left + right) / 2;
      // swap values based on where they should go in the array
      if (values[center] < values[left])
      {
         swap(values, left, center);
      }
      if (values[right] < values[left])
      {
         swap(values, left, right);
      }
      if (values[right] < values[center])
      {
         swap(values, center, right);
      }
      swap(values, center, right - 1);
      return values[right - 1];
   }
}
